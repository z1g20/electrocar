package com.grupo.ejemplo1.datos;

import com.grupo.ejemplo1.modelo.Lugar;

public interface RepositorioLugares {

    Lugar elemento(int id); //Devuelve el elemento dado su id
    void añadir(Lugar lugarcito); //Añade el elemento indicado
    int nuevo(); //Añade un elemento en blanco y devuelve id
    void borrar(int id); //Eliminar el elemento con el id
    int tamaño(); //Calcular el numero de elementos
    void actualizar(int id, Lugar lugarcito ); //Reemplaza el elemento

}
