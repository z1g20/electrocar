package com.grupo.ejemplo1.datos;

import com.grupo.ejemplo1.modelo.Lugar;
import com.grupo.ejemplo1.modelo.TipoLugar;

import java.util.ArrayList;

public class LugaresLista implements RepositorioLugares {

    protected ArrayList<Lugar> listaLugares;

    public LugaresLista() {
        listaLugares = new ArrayList<>();
        añadeEjemplos();
    }



    @Override
    public Lugar elemento(int id) {
        return listaLugares.get(id);
    }

    @Override
    public void añadir(Lugar lugarcito) {
        listaLugares.add(lugarcito);
        System.out.println(listaLugares);
    }

    @Override
    public int nuevo() {
        Lugar lugarcito2 = new Lugar();
        listaLugares.add(lugarcito2);
        return listaLugares.size()-1;

    }

    @Override
    public void borrar(int id) {
        listaLugares.remove(id);
    }

    @Override
    public int tamaño() {
        return listaLugares.size();
    }

    @Override
    public void actualizar(int id, Lugar lugarcito) {
        listaLugares.set(id, lugarcito);
    }

    public void añadeEjemplos(){
        añadir(new Lugar("UIS","Calle 9 # 27, Bucaramanga, Santander",
                -73.121,7.1377, TipoLugar.EDUCACION, 6344000, "https://www.uis.edu.co/",
                "Una de las mejores universidades de Colombia",5));

        añadir(new Lugar("Estadio Atanasio Girardot","Cra. 74 # 48010, Medellín, Antioquia",-75.59013,6.256864, TipoLugar.DEPORTE, 0,"http://comunasdemedellin.com/", "Estadio de la ciudad de medellín",
        (float) 4.5));

        añadir(new Lugar("Movistar Arena","Diagonal. 61c #26-36, Bogotá, Cundinamarca", -74.07695,4.64888, TipoLugar.ESPECTACULO, 5470183,
                "https://movistararena.co/","Centro de eventos en Bogotá",4));

        añadir(new Lugar("Páramo de Santurbán","Silos, Santander",
                -72.90982,7.2466845, TipoLugar.NATURALEZA,0, "SIN DATOS","Lugar entre los departamentos Santander y Norte de Santander",
                4));

        añadir(new Lugar("Loma Mesa de Ruitoque","Loma Mesa de Ruitoque, Floridablanca, Santander", 0,0, TipoLugar.BAR, 0, "SIN DATOS",
                "Lugar en Floridablanca, Santander",4));
    }
}
