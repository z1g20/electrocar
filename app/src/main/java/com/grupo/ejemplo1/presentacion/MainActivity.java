package com.grupo.ejemplo1.presentacion;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.grupo.ejemplo1.Aplicacion;
import com.grupo.ejemplo1.R;
import com.grupo.ejemplo1.casos_uso.CasosUsoLugar;
import com.grupo.ejemplo1.casos_uso.CasosUsosActividades;
import com.grupo.ejemplo1.datos.RepositorioLugares;
import com.grupo.ejemplo1.presentacion.AcercaDeActivity;


public class MainActivity extends AppCompatActivity {
    //Atributos de MainActivity
    TextView mensaje;
    private Button btnSalir;

    private RepositorioLugares lugares;
    private CasosUsoLugar usoLugar;

    private CasosUsosActividades usoActividades;

    //Metodos de MainActivity

    //Metodo para que cuando se de clic al boton Acerca de haga lo siguiente
    public void lanzarAcercaDe(View v){
        Intent abrir = new Intent(this, AcercaDeActivity.class);
        startActivity(abrir);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.ajustes){
            Log.d("Tag en Main","Clic en la opcion ajustes");
            return true;
        }
        if (id == R.id.acercaDe){
            usoActividades.lanzarAcercaDe();
            return true;
        }
        if (id == R.id.menu_buscar){
            Log.d("Tag main","clic a la opcion buscar");
            usoActividades.lanzarAcercaDe();
            return true;
        }
        if (id == R.id.menu_usuario){
            return true;
        }
        if (id == R.id.menu_mapa){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //barra de acciones
        Toolbar toolbar = findViewById(R.id.toolbar_Main);
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout toolbarLayout = findViewById(R.id.toolbar_layout_Main);
        toolbar.setTitle(getTitle());

        //Funcionalidad de ese boton +
        FloatingActionButton fab = findViewById(R.id.floatingActionButton);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, R.string.mensaje_fab, Snackbar.LENGTH_LONG)
                        .setAction("Accion", null).show();
            }
        });

       /* LugaresLista lugares = new LugaresLista();
        lugares.añadeEjemplos();
        for (int i = 0; i < lugares.tamaño(); i++) {
            System.out.println(i + lugares.elemento(i).toString() );
        Log.d("Tag en Main","Mensaje prueba por el logcat"+lugares.toString());
        }


       /* Lugar lugar = new Lugar("UIS","Calle 9#27",
                7.1377,-73.121, TipoLugar.EDUCACION, 6344000,
                "https://www.uis.edu.co/", "Una de las mejores universidades de Colombia",
                5);

        //mensaje = findViewById(R.id.txtNombre);
        //mensaje.setText(lugar.toString()); */


        Log.d("Tag en Main Activity", "Bienvenidos triupulantes c4");

        //boton salir
        btnSalir=findViewById(R.id.button04);
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        lugares = ((Aplicacion)getApplication()).lugares;
        usoLugar = new CasosUsoLugar(this,lugares);

        usoActividades = new CasosUsosActividades(this);

    }
}