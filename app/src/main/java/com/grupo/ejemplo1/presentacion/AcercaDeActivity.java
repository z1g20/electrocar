package com.grupo.ejemplo1.presentacion;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.grupo.ejemplo1.R;

public class AcercaDeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acercade);
    }
}
