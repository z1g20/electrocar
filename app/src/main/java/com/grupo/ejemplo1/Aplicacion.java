package com.grupo.ejemplo1;

import android.app.Application;

import com.grupo.ejemplo1.datos.LugaresLista;
import com.grupo.ejemplo1.datos.RepositorioLugares;

public class Aplicacion extends Application {

    public RepositorioLugares lugares = new LugaresLista();

    @Override public void onCreate() {
        super.onCreate();
    }
    //get de repositoriolugares
    public RepositorioLugares getLugares() {
        return lugares;
    }

}
